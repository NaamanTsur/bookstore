﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bookstore.Migrations
{
    public partial class ManyUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Franchise");

            migrationBuilder.AlterColumn<float>(
                name: "Rating",
                table: "Book",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "Branch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Country = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Street = table.Column<string>(nullable: false),
                    BuildingNumber = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Owner = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    OpeningHours = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branch", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "7d2ed6c5-3ce4-4bc7-a140-054a05a15ada");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "381ce661-0b49-4a4b-b90b-493cad5e7daf");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 9f, new DateTime(2019, 8, 29, 23, 54, 37, 600, DateTimeKind.Local).AddTicks(6265) });

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 10f, new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253) });

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 7f, new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253) });

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 8f, new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253) });

            migrationBuilder.InsertData(
                table: "Branch",
                columns: new[] { "Id", "BuildingNumber", "City", "Country", "Name", "OpeningHours", "Owner", "Phone", "Street" },
                values: new object[,]
                {
                    { 1, 2, "Rishon Lezion", "Israel", "Bookstore Of Managmenet", "08:00 - 20:00", "Doctor Shakshuka", "03-9634390", "HaDayagim" },
                    { 2, 4, "Tel Aviv", "Israel", "Bookstore Of Parties", "09:00 - 18:00", "Lady Luck", "052-0522250", "Shaul Hamelech" },
                    { 3, 8, "Eilat", "Israel", "Bookstore Of Seas", "06:00 - 17:00", "Mister Surfer", "050-2386234", "Agmonim" },
                    { 4, 1, "Tiberias", "Israel", "Bookstore Of Nothing", "11:00 - 22:00", "Cool Chiller", "08-9423665", "HaKishon" },
                    { 5, 26, "Haifa", "Israel", "Bookstore Of Spades", "08:00 - 21:00", "Mad Scientist", "04-5371945", "Tchernichovski" }
                });

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Branch");

            migrationBuilder.AlterColumn<int>(
                name: "Rating",
                table: "Book",
                nullable: false,
                oldClrType: typeof(float));

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BuildingNumber = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    OpeningHours = table.Column<string>(nullable: false),
                    Owner = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    Street = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "ae56cd58-965b-44e7-845e-0e14ab012d89");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "dbee7208-2c68-4ced-90c9-a3d0b719e96c");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 9, new DateTime(2019, 8, 21, 7, 54, 9, 540, DateTimeKind.Local).AddTicks(7516) });

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 10, new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502) });

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 7, new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502) });

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Rating", "ReleaseDate" },
                values: new object[] { 8, new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502) });

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502));

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "BuildingNumber", "City", "Country", "Name", "OpeningHours", "Owner", "Phone", "Street" },
                values: new object[,]
                {
                    { 1, 2, "Rishon Lezion", "Israel", "Bookstore Of Managmenet", "08:00 - 20:00", "Doctor Shakshuka", "03-9634390", "Eli Vizel" },
                    { 2, 12, "Tel Aviv", "Israel", "Bookstore Of Parties", "09:00 - 18:00", "Lady Luck", "052-0522250", "Rothschild Boulevard" },
                    { 3, 8, "Eilat", "Israel", "Bookstore Of Seas", "06:00 - 17:00", "Mister Surfer", "050-2386234", "Shderot HaTmarim" },
                    { 4, 1, "Tiberias", "Israel", "Bookstore Of Nothing", "11:00 - 22:00", "Cool Chiller", "08-9423665", "HaKishon" },
                    { 5, 26, "Haifa", "Israel", "Bookstore Of Spades", "08:00 - 21:00", "Mad Scientist", "04-5371945", "Tchernichovski" }
                });
        }
    }
}
