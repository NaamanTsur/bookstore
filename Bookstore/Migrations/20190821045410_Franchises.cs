﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bookstore.Migrations
{
    public partial class Franchises : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Country = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Street = table.Column<string>(nullable: false),
                    BuildingNumber = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Owner = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    OpeningHours = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "ae56cd58-965b-44e7-845e-0e14ab012d89");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "dbee7208-2c68-4ced-90c9-a3d0b719e96c");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 540, DateTimeKind.Local).AddTicks(7516));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 21, 7, 54, 9, 542, DateTimeKind.Local).AddTicks(7502));

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "BuildingNumber", "City", "Country", "Name", "OpeningHours", "Owner", "Phone", "Street" },
                values: new object[,]
                {
                    { 1, 2, "Rishon Lezion", "Israel", "Bookstore Of Managmenet", "08:00 - 20:00", "Doctor Shakshuka", "03-9634390", "Eli Vizel" },
                    { 2, 12, "Tel Aviv", "Israel", "Bookstore Of Parties", "09:00 - 18:00", "Lady Luck", "052-0522250", "Rothschild Boulevard" },
                    { 3, 8, "Eilat", "Israel", "Bookstore Of Seas", "06:00 - 17:00", "Mister Surfer", "050-2386234", "Shderot HaTmarim" },
                    { 4, 1, "Tiberias", "Israel", "Bookstore Of Nothing", "11:00 - 22:00", "Cool Chiller", "08-9423665", "HaKishon" },
                    { 5, 26, "Haifa", "Israel", "Bookstore Of Spades", "08:00 - 21:00", "Mad Scientist", "04-5371945", "Tchernichovski" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Franchise");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "3a515ffa-0d26-4021-9e1a-f19a5f48eaac");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "fe7b9e53-3c85-4fd1-bb11-0c7f8767ca18");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 64, DateTimeKind.Local).AddTicks(3288));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));
        }
    }
}
