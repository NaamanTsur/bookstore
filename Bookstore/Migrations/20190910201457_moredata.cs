﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bookstore.Migrations
{
    public partial class moredata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "6f6d4abd-b647-4b2a-bd68-36a0ecd4d98f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "cd790960-7c61-42d3-a4d4-5862d8189bee");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 9, 10, 23, 14, 56, 550, DateTimeKind.Local).AddTicks(6630));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615));

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "Id", "AgeRangeMax", "AgeRangeMin", "Author", "Genre", "Price", "Quantity", "Rating", "ReleaseDate", "Title" },
                values: new object[,]
                {
                    { 5, 55, 16, "F. C. Yee", "Fantasy", 67, 24, 8f, new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615), "The rise of Kyoshi" },
                    { 6, 99, 6, "J. R. R. Tolkien", "Fantasy", 30, 18, 8f, new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615), "The Hobbit" },
                    { 7, 67, 18, "H. P. Lovecraft", "Fantasy", 23, 45, 8f, new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615), "The Call of Cthulhu" },
                    { 8, 33, 16, "Someone Someonegton", "Horror", 54, 35, 8f, new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615), "Dexter" },
                    { 10, 67, 18, "Coder Coddington", "Programming", 128, 37, 8f, new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615), "Effective Java" },
                    { 9, 67, 18, "Uncle Bob", "Programming", 80, 42, 8f, new DateTime(2019, 9, 10, 23, 14, 56, 552, DateTimeKind.Local).AddTicks(6615), "Clean Code" }
                });

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 9, 10, 23, 14, 56, 553, DateTimeKind.Local).AddTicks(6609));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 9, 10, 23, 14, 56, 553, DateTimeKind.Local).AddTicks(6609));

            migrationBuilder.InsertData(
                table: "Customer",
                columns: new[] { "Id", "BirthDate", "Email", "JoinDate", "Name", "Phone" },
                values: new object[,]
                {
                    { 3, new DateTime(1997, 12, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "naaman123@gmail.com", new DateTime(2019, 9, 10, 23, 14, 56, 553, DateTimeKind.Local).AddTicks(6609), "Naaman Tsur", "054-342342" },
                    { 4, new DateTime(1999, 1, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "liroasdasd@gmail.com", new DateTime(2019, 9, 10, 23, 14, 56, 553, DateTimeKind.Local).AddTicks(6609), "Lior Somdit", "192-123591" },
                    { 5, new DateTime(1968, 8, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "southerntemoke@gmail.com", new DateTime(2019, 9, 10, 23, 14, 56, 553, DateTimeKind.Local).AddTicks(6609), "Kelsang forbes", "152-1637894" },
                    { 6, new DateTime(1965, 9, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "yokoyarocks@gmail.com", new DateTime(2019, 9, 10, 23, 14, 56, 553, DateTimeKind.Local).AddTicks(6609), "Jianzhu thumb", "152-9182734" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "7d2ed6c5-3ce4-4bc7-a140-054a05a15ada");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "381ce661-0b49-4a4b-b90b-493cad5e7daf");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 600, DateTimeKind.Local).AddTicks(6265));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 29, 23, 54, 37, 602, DateTimeKind.Local).AddTicks(6253));
        }
    }
}
