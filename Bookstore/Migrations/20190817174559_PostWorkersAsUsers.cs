﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bookstore.Migrations
{
    public partial class PostWorkersAsUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "0ae8cf84-7aee-409e-88ea-cee5efe66f1a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "578ba154-7b70-4631-a4f4-7baa808a0b62");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 73, DateTimeKind.Local).AddTicks(5529));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "0fd1f6da-0458-4b8a-a309-92dcd4e753e7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "14d688dd-2258-4d41-b7c2-269899d4103c");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 18, 46, 59, 520, DateTimeKind.Local).AddTicks(9414));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 18, 46, 59, 523, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 18, 46, 59, 523, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 18, 46, 59, 523, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 17, 18, 46, 59, 523, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 17, 18, 46, 59, 523, DateTimeKind.Local).AddTicks(9377));
        }
    }
}
