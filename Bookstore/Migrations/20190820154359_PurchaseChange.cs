﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bookstore.Migrations
{
    public partial class PurchaseChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalCost",
                table: "Purchase",
                newName: "PaidPrice");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "3a515ffa-0d26-4021-9e1a-f19a5f48eaac");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "fe7b9e53-3c85-4fd1-bb11-0c7f8767ca18");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 64, DateTimeKind.Local).AddTicks(3288));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 20, 18, 43, 59, 66, DateTimeKind.Local).AddTicks(3279));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PaidPrice",
                table: "Purchase",
                newName: "TotalCost");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "0ae8cf84-7aee-409e-88ea-cee5efe66f1a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "578ba154-7b70-4631-a4f4-7baa808a0b62");

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 1,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 73, DateTimeKind.Local).AddTicks(5529));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 2,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 3,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Book",
                keyColumn: "Id",
                keyValue: 4,
                column: "ReleaseDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1,
                column: "JoinDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2,
                column: "JoinDate",
                value: new DateTime(2019, 8, 17, 20, 45, 59, 75, DateTimeKind.Local).AddTicks(5514));
        }
    }
}
