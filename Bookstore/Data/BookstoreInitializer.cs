﻿using Microsoft.AspNetCore.Identity;
using Bookstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookstore.Data
{
    public class BookstoreInititalizer
    {
        public static void SeedUsers(BookstoreUserManager userManager)
        {
            if (userManager.FindByEmailAsync("admin@bookstore.colman").Result == null)
            {
                Worker admin = new Worker
                {
                    UserName = "Admin",
                    Email = "admin@bookstore.colman",
                    Name = "Manager",
                    Phone = "000-0000000",
                    Salary = 0,
                    JoinDate = DateTime.Now
                };

                IdentityResult result = userManager.CreateAsync(admin, "Adm!n12").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(admin, "Admin").Wait();
                }
                else
                {
                    throw new Exception("Unable to create admin manager");
                }
            }

            if (userManager.FindByEmailAsync("israel@bookstore.colman").Result == null)
            {
                Worker worker = new Worker
                {
                    UserName = "Israel",
                    Email = "israel@bookstore.colman",
                    Name = "Israel Israeli",
                    Phone = "012-2345678",
                    Salary = 32,
                    JoinDate = DateTime.Now
                };

                IdentityResult result = userManager.CreateAsync(worker, "!Srael0").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(worker, "Worker").Wait();
                }
                else
                {
                    throw new Exception("Unable to create first worker");
                }
            }

            if (userManager.FindByEmailAsync("naaman@bookstore.colman").Result == null)
            {
                Worker worker = new Worker
                {
                    UserName = "Naaman",
                    Email = "naaman@bookstore.colman",
                    Name = "Naaman Naamanson",
                    Phone = "050-4422123",
                    Salary = 420,
                    JoinDate = DateTime.Now
                };

                IdentityResult result = userManager.CreateAsync(worker, "mamram123").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(worker, "Worker").Wait();
                }
                else
                {
                    throw new Exception("Unable to create second worker");
                }
            }

            if (userManager.FindByEmailAsync("liad@bookstore.colman").Result == null)
            {
                Worker worker = new Worker
                {
                    UserName = "liad",
                    Email = "liad@bookstore.colman",
                    Name = "Liad Liadson",
                    Phone = "012-9182374",
                    Salary = 1337,
                    JoinDate = DateTime.Now
                };

                IdentityResult result = userManager.CreateAsync(worker, "matzov123").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(worker, "Worker").Wait();
                }
                else
                {
                    throw new Exception("Unable to create third worker");
                }
            }

            if (userManager.FindByEmailAsync("gabi@bookstore.colman").Result == null)
            {
                Worker worker = new Worker
                {
                    UserName = "gabi",
                    Email = "gabi@bookstore.colman",
                    Name = "Gabi Gabinson",
                    Phone = "012-9182734",
                    Salary = 9877,
                    JoinDate = DateTime.Now
                };

                IdentityResult result = userManager.CreateAsync(worker, "airforce123").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(worker, "Worker").Wait();
                }
                else
                {
                    throw new Exception("Unable to create fourth worker");
                }
            }

            if (userManager.FindByEmailAsync("eden@bookstore.colman").Result == null)
            {
                Worker worker = new Worker
                {
                    UserName = "eden",
                    Email = "eden@bookstore.colman",
                    Name = "Eden Edenson",
                    Phone = "012-9182734",
                    Salary = 1923,
                    JoinDate = DateTime.Now
                };

                IdentityResult result = userManager.CreateAsync(worker, "mamram123").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(worker, "Worker").Wait();
                }
                else
                {
                    throw new Exception("Unable to create fifth worker");
                }
            }
        }
    }
}
