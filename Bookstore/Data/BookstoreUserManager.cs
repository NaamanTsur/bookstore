﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

using Bookstore.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace Bookstore.Data
{
    public class BookstoreUserManager : UserManager<Worker>
    {
        public BookstoreUserManager(IUserStore<Worker> store, 
                                                IOptions<IdentityOptions> optionsAccessor, 
                                                IPasswordHasher<Worker> passwordHasher, 
                                                IEnumerable<IUserValidator<Worker>> userValidators, 
                                                IEnumerable<IPasswordValidator<Worker>> passwordValidators, 
                                                ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, 
                                                IServiceProvider services, ILogger<UserManager<Worker>> logger) : 
            base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }
    }

    public class BookstoreSignInManager : SignInManager<Worker>
    {
        public BookstoreSignInManager(UserManager<Worker> userManager, 
                                                  IHttpContextAccessor contextAccessor, 
                                                  IUserClaimsPrincipalFactory<Worker> claimsFactory, 
                                                  IOptions<IdentityOptions> optionsAccessor, 
                                                  ILogger<SignInManager<Worker>> logger, 
                                                  IAuthenticationSchemeProvider schemes) : 
            base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
        {
        }
    }
}
